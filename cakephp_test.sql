-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2021 at 11:21 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cakephp_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `Id` int(121) NOT NULL,
  `title` varchar(121) NOT NULL,
  `details` varchar(9990) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`Id`, `title`, `details`, `created`, `modified`) VALUES
(1, 'sports', 'Ipl start on 9th april 2021, Ipl start on 9th april 2021, Ipl start on 9th april 2021,Ipl start on 9th april 2021,Ipl sta', '2021-04-11 10:20:31', '2021-04-11 10:20:31'),
(2, 'Lockdown', 'Lockdown again going to stat on 14th April, Lockdown again going to stat on 14th April, Lockdown again going to stat on 14th April, Lockdown again going to stat on 14th April, Lockdown again going to stat on 14th April, L', '2021-04-11 10:22:02', '2021-04-11 10:22:02'),
(3, 'Bariwala.com', 'Bariwala.com is an online based HouseRental system. Bariwala.com is an online based HouseRental system. Bariwala.com is an online based HouseRental system. Bariwala.com is an online based HouseRental system. ', '2021-04-11 10:28:13', '2021-04-11 10:28:13'),
(4, 'CakePhp', 'CakePhp is a Php framework, here we are using CakePhp version 4.2.5. CakePhp is a Php framework, here we are using CakePhp version 4.2.5. CakePhp is a Php framework, here we are using CakePhp version 4.2.5. CakePhp is a Php framework, here we are using CakePhp version 4.2.5. ', '2021-04-11 10:29:39', '2021-04-11 10:29:39'),
(5, 'Laravel', 'Laravel is gaint PHP framework, which follow MVC pattern, Laravel is gaint PHP framework, which follow MVC pattern, Laravel is gaint PHP framework, which follow MVC pattern, Laravel is gaint PHP framework, which follow MVC pattern, Laravel is gaint PHP framework, which follow MVC pattern, ', '2021-04-11 10:30:31', '2021-04-11 10:30:31'),
(6, 'Vue.Js', 'Vue.Js is a framework of Javascript, Vue.Js is a framework of Javascript, Vue.Js is a framework of Javascript, Vue.Js is a framework of Javascript, ', '2021-04-11 10:31:50', '2021-04-11 10:31:50');

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20210406124927, 'Users', '2021-04-06 07:32:45', '2021-04-06 07:32:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(121) NOT NULL,
  `name` varchar(121) NOT NULL,
  `age` int(121) NOT NULL,
  `country` varchar(121) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `age`, `country`, `created_at`, `updated_at`) VALUES
(1, 'Karim', 23, 'Bangladesh', '2021-04-06 13:43:58', '0000-00-00 00:00:00'),
(2, 'Parvez', 33, 'India', '2021-04-06 13:47:59', '0000-00-00 00:00:00'),
(3, 'Ariful Islam', 43, 'Bangladesh', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Arman', 33, 'Iran', '2021-04-10 07:35:46', '2021-04-10 07:35:46'),
(5, 'Kamrul Hasan', 44, 'Bangladesh', '2021-04-10 07:36:01', '2021-04-10 07:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(240) NOT NULL,
  `active` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `image`, `active`, `created`, `modified`) VALUES
(4, 'admin', '', '$2y$10$bp4T66QQg7ljC.ls08pA/e8PC8v/yP89IufpmodiftypgKLMQci5u', '', 0, '2021-04-12 07:10:45', '2021-04-12 07:10:45'),
(5, 'user', '', '$2y$10$6MM9mwxC05yuPcIdftPRCOlFimDx1LlXf3.REvzkH3pin2pNSabUy', '', 0, '2021-04-19 04:38:29', '2021-04-19 04:38:29'),
(6, 'suzan', '', '$2y$10$shyJJeVqg4lNIoBpr7rFsO7gMiNtaIkmXqq/XWksU7w3Hunnrgerq', '', NULL, '2021-04-20 05:04:51', '2021-04-20 05:04:51'),
(7, 'aaa', '', '$2y$10$1oL50PH4S/ad/MN4UgOIsuLesOwog/8HkikutL6jLKST17xFRWPkG', '', NULL, '2021-04-20 05:06:24', '2021-04-20 05:06:24'),
(8, 'bbb', '', '$2y$10$6UWVtbWppC24t9Q2GCKb3.wmngogUzaxEjG7Slxua5clXA3w2vcvK', '', NULL, '2021-04-20 05:09:38', '2021-04-20 05:09:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `Id` int(121) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(121) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
