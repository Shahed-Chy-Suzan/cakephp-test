<!--
    ?php
    $this->Breadcrumbs->add(
        'Home',
        ['controller' => 'Blogs', 'action' => 'home'],
        ['class'=> 'breadcrumb-item active']
    );

?> -->


<div class="container">

<div class="row">
    <div class="col-4">
        <h3 style="color: #fff;" class="bg-info text-capitalize p-1">Recent Post</h3>
        <ul class="list-group list-group-flush">
            <?php foreach ($articleList as $key => $articleTitle): ?>
                <li class="list-group-item">
                    <a href =<?= $this->Url->build(['controller'=>'Blogs','action'=>'view',$key])?>>
                        <?= $articleTitle ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="col-8">
        <div class="row">
            <div class="list-group ">
                <?php foreach ($articles as $key => $article): ?>

                <a href=<?= $this->Url->build(['controller'=>'Blogs','action'=>'view',$article->id]) ?> class="list-group-item list-group-item-action flex-column mb-2">
                  <div class="d-flex w-100 justify-content-between">
                      <h5 class="mb-1 mr-3"><?= $article->title ?></h5>
                      <h5 class="mb-1 mr-3"><?= $article->id ?></h5>
                      <small class="px-3">3 days ago</small>
                  </div>
                  <p class="mb-1">
                      <?=
                      $this->Text->truncate(                  //make: description see more
                          $article->details,
                          140,
                          [
                              'ellipsis' => '...',
                              'exact' => true
                          ]
                      );
                      ?>
                  </p>
                </a>

                <?php endforeach; ?>

                <ul class = 'pagination justify-content-center'>                 <!--------for pagination-------->
                    <?= $this->Paginator->prev("<<") ?>

                    <?= $this->Paginator->numbers() ?>

                    <?= $this->Paginator->next(">>") ?>
                </ul>

            </div>

        </div>
    </div>
</div>


    <!-- ============ adding Tinymce ================= -->
<script src="https://cdn.tiny.cloud/1/z0trcmjcebnz3oxh16vbmaojmpyse6twblaywvm9nsqv5exr/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
    selector: '#mytextarea'
    });
</script>

<h1>TinyMCE Quick Start Guide</h1>
    <form method="post">
      <textarea id="mytextarea">Hello, World!</textarea>
</form>

<hr>


<textarea>
    Welcome to TinyMCE!
</textarea>
  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      toolbar_mode: 'floating',
   });
  </script>
      <!-- ============ adding Tinymce ================= -->


</div>
