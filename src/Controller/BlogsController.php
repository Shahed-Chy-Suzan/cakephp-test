<?php

namespace App\Controller;

use cake\Event\EventInterface;                      //used for global nav
use Cake\Utility\Text;
use Cake\Http\Client;

class BlogsController extends AppController{

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('blog');

        // $this->loadModel('Menus');

        // $menus = $this->Menus->find('all',['contain'=>['Submenus']]);

        // $this->set('menus', $menus);
        // debug($event);
        // exit;
    }

    public function home()
    {
        // $this->viewBuilder()->setLayout('ajax');    //cakephp er default nav ta show korbe na,(without nav)
        //$this->viewBuilder()->setLayout('blog');       //will show blog nav(now globally)

        // exit;
        $this->loadModel('Articles');          //bcz we'r in BlogsController but using Articles

        $articles = $this->Articles->find()
                                   ->order(['Articles.id DESC']);       //find() for all data from DB

        $articleList = $this->Articles->find('list')->limit('8');    //find('list') for key field(id,title) data from DB
        //$articleList = $this->Articles->find('list',['valueField'=>'created','keyField'=>'title']);
            //it will show created field as a title_list, & show 'title' instead of 'id' in route_url

        $this->set('articles',$this->paginate($articles,['limit'=>'3']));       //for pagination

        $this->set('articleList',$articleList);

    }

    public function about()
    {
        //about
    }

    public function contact()
    {
        exit("Hello");
    }

    public function view($id = null)
    {
        // exit($id);
        $this->loadModel('Articles');
        $article = $this->Articles->get($id);
        // debug($article);
        // exit;
        // dd($article);
        $this->set('article',$article);
    }

}
